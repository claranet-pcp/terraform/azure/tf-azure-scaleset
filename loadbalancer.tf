# Front End Load Balancer
resource "azurerm_lb" "load_balancer" {
  name                = "azu-${var.environment}-${var.name}-lb"
  location            = "${var.region}"
  resource_group_name = "${var.resource_group}"

  frontend_ip_configuration {
    name                          = "azu-${var.environment}-${var.name}-lb-ipconfig"
    subnet_id                     = "${var.subnet}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${var.lb_ip}"
  }

  tags {
    environment   = "${var.environment}"
    service       = "${var.name}"
    configuration = "${var.configuration}"
    terraform     = "true"
  }
}

# Back End Address Pool
resource "azurerm_lb_backend_address_pool" "backend_pool" {
  resource_group_name = "${var.resource_group}"
  loadbalancer_id     = "${azurerm_lb.load_balancer.id}"
  name                = "azu-${var.environment}-${var.name}-backend_address_pool"
}

# Load Balancer Rule
resource "azurerm_lb_rule" "load_balancer_rule" {
  count                          = "${length(split(",", var.ports))}"
  resource_group_name            = "${var.resource_group}"
  loadbalancer_id                = "${azurerm_lb.load_balancer.id}"
  name                           = "azu-${var.environment}-${var.name}-lbrule-Port-${element(split(",",var.ports), count.index)}"
  protocol                       = "Tcp"
  frontend_port                  = "${element(split(",",var.ports), count.index)}"
  backend_port                   = "${element(split(",",var.ports), count.index)}"
  frontend_ip_configuration_name = "azu-${var.environment}-${var.name}-lb-ipconfig"
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.backend_pool.id}"
  probe_id                       = "${azurerm_lb_probe.load_balancer_probe.id}"
  load_distribution              = "SourceIP"
  depends_on                     = ["azurerm_lb_probe.load_balancer_probe"]
}

#LB Probe - Checks to see which VMs are healthy and available
resource "azurerm_lb_probe" "load_balancer_probe" {
  resource_group_name = "${var.resource_group}"
  loadbalancer_id     = "${azurerm_lb.load_balancer.id}"
  name                = "azu-${var.environment}-${var.name}-probe-port-${element(split(",",var.ports), 0)}"
  port                = "${element(split(",",var.ports), 0)}"
}
