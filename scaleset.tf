resource "azurerm_virtual_machine_scale_set" "scaleset" {
  name                = "azu-web-${substr(var.environment, 0,1)}-int-${var.name}-ss"
  location            = "${var.region}"
  resource_group_name = "${var.resource_group}"
  upgrade_policy_mode = "Manual"
  priority            = "Regular"

  sku {
    name     = "${var.scaleset_size}"
    tier     = "Standard"
    capacity = "${var.scaleset_instances}"
  }

  storage_profile_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  storage_profile_os_disk {
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_profile_data_disk {
    lun               = 0
    caching           = "ReadWrite"
    create_option     = "Empty"
    disk_size_gb      = 128
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name_prefix = "${var.machine_prefix}"
    admin_username       = "${var.admin_username}"
    admin_password       = "${var.admin_password}"
  }

  os_profile_windows_config {
    provision_vm_agent        = true
    enable_automatic_upgrades = true
  }

  network_profile {
    name    = "${var.name}networkprofile"
    primary = true

    ip_configuration {
      name                                   = "${var.name}IPConfiguration"
      subnet_id                              = "${var.subnet}"
      primary                                = true
      load_balancer_backend_address_pool_ids = ["${azurerm_lb_backend_address_pool.backend_pool.id}"]
    }
  }

  extension {
    name                 = "IaaSAntimalware"
    publisher            = "Microsoft.Azure.Security"
    type                 = "IaaSAntimalware"
    type_handler_version = "1.5"

    settings = <<SETTINGS
    {
      "AntimalwareEnabled": true, 
      "RealtimeProtectionEnabled": true, 
      "ScheduledScanSettings": { 
        "isEnabled": true, 
        "day": "1", 
        "time": "120", 
        "scanType": "Full"
      }, 
      "Exclusions": { 
        "Extensions": ".mdf;.ldf", 
        "Paths": "c:\\windows;c:\\windows\\system32", 
        "Processes": "taskmgr.exe;notepad.exe"
      }
    }
SETTINGS
  }

  extension {
    name                 = "dcjoin"
    publisher            = "Microsoft.Compute"
    type                 = "JsonADDomainExtension"
    type_handler_version = "1.0"

    settings = <<SETTINGS
    {
      "Name": "${var.domain_name}",
      "OUPath": "",
      "User": "${var.domain_name}\\${var.domain_adminuser}",
      "Restart": "true",
      "Options" :  "3"
    }
SETTINGS

    protected_settings = <<SETTINGS
    {
      "Password":"${var.domain_adminpass}"
    }
SETTINGS
  }

  extension {
    name                 = "vmdiag-ext"
    publisher            = "Microsoft.Azure.Diagnostics"
    type                 = "IaaSDiagnostics"
    type_handler_version = "1.5"

    settings = <<SETTINGS
    {
      "xmlCfg": "${base64encode(data.template_file.xml.rendered)}",
      "storageAccount": "${var.diagstorageaccountname}"
    }
SETTINGS

    protected_settings = <<SETTINGS
    {
      "storageAccountName": "${var.diagstorageaccountname}",
      "storageAccountKey": "${var.diagstorageaccountkey}"
    }
SETTINGS
  }

  extension {
    name                       = "${var.name}OMSExtension"
    publisher                  = "Microsoft.EnterpriseCloud.Monitoring"
    type                       = "MicrosoftMonitoringAgent"
    type_handler_version       = "1.0"
    auto_upgrade_minor_version = true

    settings = <<SETTINGS
    {
      "workspaceId":  "${var.oms_workspace_id}"
    }
SETTINGS

    protected_settings = <<PROTECTED_SETTINGS
    {
      "workspaceKey": "${var.oms_workspace_key}"
    }
PROTECTED_SETTINGS
  }
}

data "template_file" "xml" {
  template = "${file("${path.module}/windows-config.xml.tpl")}"
}
