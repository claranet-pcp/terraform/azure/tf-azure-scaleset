variable "scaleset_size" {}
variable "scaleset_instances" {}
variable "admin_username" {}
variable "admin_password" {}
variable "name" {}
variable "resource_prefix" {}
variable "machine_prefix" {}
variable "subnet" {}
variable "lb_ip" {}
variable "ports" {}
variable "region" {}
variable "resource_group" {}

variable "oms_workspace_id" {}

variable "oms_workspace_key" {}

variable "environment" {}
variable "configuration" {}

variable "domain_name" {}
variable "domain_adminuser" {}
variable "domain_adminpass" {}

variable "diagstorageaccountname" {}

variable "diagstorageaccountkey" {}
